# inChurch Developer Recruitment 2020  #

### Setting up the project environment ###

Firstly, a virtual environment should be created using the virtualenv python library. If you don't have it installed, just run:

```pip install virtualenv```
```virtualenv <virtual env name>```

After that, you should activate the virtual environment:

```source <virtual env name>bin/activate```

And then you can install all the requirements, by running:

```pip install -r requirements.txt```

Finally, run the project by moving to the "backend" folder, and running:

```python manage.py runserver```

### Setting up the project environment ###

* Endpoint for user's registration - Access url "auth/create/"
* Endpoint for user's authentication - Access url "login/"
* Endpoint to read the user profile list - Access url "auth/list/"
* Endpoint to read the user profile detail - Access url "auth/info/<profileId>"
* Endpoint to update a user profile - Access url "auth/update/<profileId>"
* Endpoint to delete a user profile - Access url "auth/delete/<profileId>"
* Endpoint to create a department - Access url "department/create/"
* Endpoint to read the department detail - Access url "department/list/"
* Endpoint to read the department detail - Access url "department/info/<departmentId>"
* Endpoint to update a department - Access url "department/update/<departmentId>"
* Endpoint to delete a department - Access url "department/delete/<departmentId>"

### API Documentation and Testing ###

Can be accessed at the url "docs", which shows every endpoint created, allowing the user to test each one. Rest Framework Swagger was used to make it easier to implement the project documentation.

When talking about the tests, just few of them were implemented. Part of it is because of the time spent in other system features. However, some useful links and libraries were found, in order to help for a future implementation of all the unit tests:

https://realpython.com/test-driven-development-of-a-django-restful-api/
https://realpython.com/testing-in-django-part-1-best-practices-and-examples/
https://coverage.readthedocs.io/en/coverage-5.0.3/

To run the tests, this code should be executed in the command line (it will execute the "tests.py file"):

```python manage.py test```

### Authorization Method ###

Token authorization was used, which implies in testing some of the urls in programs like "Postman", passing the token of the logged user as a header:

```Authorization: Token <insert token string here>```