from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import Group
from django.forms.models import model_to_dict
from auth_profile.models import Profile
from auth_profile.serializers import ProfileSerializer

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def list(request):
    if request.method == 'GET':
        profiles = Profile.objects.all()
        serializer = ProfileSerializer(profiles, many=True)
        return Response(serializer.data)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def create(request):
    if request.method == 'POST':
        serializer = ProfileSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            ## Get profile from save method return,
            ## then add its token to the response
            profile = serializer.save()
            token = Token.objects.get(user=profile).key
            data["profile"] = serializer.data
            data["response"] = "User successfully created"
            data["token"] = token

            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update(request, profileId):
    try:
        profile = Profile.objects.get(id=profileId)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    notFromSameDpt = request.user.departmentId != profile.departmentId
    nonAdmin = not request.user.is_staff
    condition = nonAdmin and notFromSameDpt
    if condition:
        return Response({"response": """You're just allowed to update profiles 
                                        from your department"""})

    if request.method == 'PUT':
        serializer = ProfileSerializer(profile, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def retrieve(request, profileId):
    try:
        profile = Profile.objects.get(id=profileId)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProfileSerializer(profile)
        return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def delete(request, profileId):
    try:
        profile = Profile.objects.get(id=profileId)
    except Profile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    isNotLoggedUser = request.user.id != profile.id
    nonAdmin = not request.user.is_staff
    condition = nonAdmin and isNotLoggedUser
    if condition:
        return Response({"response": "You can't delete other's profile"})

    data = {}
    if request.method == 'DELETE':
        profile.delete()
        serializer = ProfileSerializer(profile)
        data["profile"] = serializer.data
        data["response"] = "Profile successfully deleted"
        return Response(data)
    return Response(status=status.HTTP_400_BAD_REQUEST)