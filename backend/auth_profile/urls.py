from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from .views import *

urlpatterns = [
    ## Rest Framework urls
	path('login/', obtain_auth_token, name='login'),

	## Profile URLs
    path('auth/list/', profile.list, name='users_list'),
    path('auth/create/', profile.create, name='user_creation'),
    path('auth/update/<profileId>/', profile.update, name='user_update'),
    path('auth/info/<profileId>/', profile.retrieve, name='user_info'),
    path('auth/delete/<profileId>/', profile.delete, name='user_deletion'),

    # Department URLs
    path('department/list/', department.list, name='departments_list'),
    path('department/create/', department.create, name='department_creation'),
    path('department/update/<depId>/', department.update, name='department_update'),
    path('department/info/<depId>/', department.retrieve, name='department_info'),
    path('department/delete/<depId>/', department.delete, name='department_deletion'),
]