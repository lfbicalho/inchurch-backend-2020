from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

class Department(models.Model):
	name = models.CharField(max_length=100, null=False)

	def __str__(self):
		return  self.name

class Profile(AbstractUser):
	REQUIRED_FIELDS = ()
	USERNAME_FIELD = 'email'

	username = None
	name = models.CharField(max_length=70, null=False)
	email = models.CharField(max_length=100, null=False, unique=True)
	password = models.CharField(max_length=20, null=False)
	is_staff = models.BooleanField(default=False)
	departmentId = models.ForeignKey(Department, null=True, on_delete=models.SET_NULL)

@receiver(post_save, sender=Profile)
def create_token(sender, instance=None, created=False, **kwargs):
	if created:
		token = Token.objects.create(user=instance)