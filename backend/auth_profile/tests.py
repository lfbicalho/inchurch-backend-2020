from django.test import SimpleTestCase
from django.urls import reverse, resolve
from .views import *

class TestUrls(SimpleTestCase):
	## Testing list methods
	def test_profile_list_url(self):
		url = reverse('users_list')
		self.assertEquals(resolve(url).func, profile.list)

	def test_department_list_url(self):
		url = reverse('departments_list')
		self.assertEquals(resolve(url).func, department.list)

	## Testing retrieve methods
	# def test_profile_info_url(self):
	# 	url = reverse('user_info')
	# 	self.assertEquals(resolve(url, kwargs={'profileId':14}).func, profile.retrieve)

	# def test_department_info_url(self):
	# 	url = reverse('department_info')
	# 	self.assertEquals(resolve(url, kwargs={'depId':1}).func, department.retrieve)