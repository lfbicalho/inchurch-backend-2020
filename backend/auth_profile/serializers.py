from rest_framework import serializers
from django.contrib.auth.models import User
from .models import *

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = [
            'id',
        	'name',
            'email',
            'password',
            'departmentId',
            'is_staff',
        ]
        extra_kwargs={
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        profile = Profile(
            email = validated_data["email"],
            name = validated_data["name"],
        )
        profile.set_password(validated_data["password"])
        profile.save()
        return profile

class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = [
            'id',
            'name',
        ]